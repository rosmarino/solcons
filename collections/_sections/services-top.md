---
page: services-top # page-section
order: 1
DIVid: home # this gives the div its id
include: sections/banner.html
# banner image info
img-ref: https://res.cloudinary.com/assaporami/image/upload/q_auto:eco/v1586446144/solcons/calcolatore-1600x1100_iuipmj.jpg
img-maxhght: 400px # px or %
img-alt: servizi professionali
# logo info | only requred for LOGO txt-type
logo-ref: 
logo-alt: 
# this is actually the header text; txt-type chooses the delivery style
txt: i nostri servizi
txt-type: HDR
# use:
    # HDR for header: it will be uppercased
    # TAG for tag
    # LOGO for logo
    # CARD for complex card
# card extra variables:
claim: 
phone: "040.24.64.081"
email: info@solcons.it
---
{% comment %}
All info in the front matter - no text here
{% endcomment %}