---
page: homepage-top # page-section
order: 1
DIVid: home # this gives the div its id
include: sections/banner.html
# banner image info
img-ref: https://res.cloudinary.com/assaporami/image/upload/q_auto:eco/v1586105694/solcons/desktop-1600x1100_nkibtg.jpg
img-maxhght: 600px # px or %
img-alt: studio di commercialisti
# logo info | only requred for LOGO txt-type
logo-ref: 
logo-alt: 
# this is actually the header text; txt-type chooses the delivery style
txt: Solutions Consulting
txt-type: CARD
# use:
    # HDR for header: it will be uppercased
    # TAG for tag
    # LOGO for logo
    # CARD for complex card
# card extra variables:
claim: Studio professionale di Dottori Commercialisti a Trieste e Udine   
phone: "040.24.64.081"
email: info@solcons.it
---
{% comment %}
All info in the front matter - no text here
{% endcomment %}