---
layout: memo
title:  "A cosa serve questa sezione"
subtitle: "Questa è il sottotitolo, in genere non più di una riga"
date: 2020-04-10 14:00:00 +0200
categories:
tags: [fiscale, societario]
author: "Marco"
---
Questo è un memo esplicativo del possibile utilizzo di questa sezione, che permette di scrivere con semplicità e pubblicare in un istante contenuti come note sulle norme fiscali, opportunità, commenti, ecc.

## testo vero è proprio.
Il primo paragrafo viene ripreso automaticamente come *abstract* nella pagina indice. Questo è il secondo. Si possono usare il *corsivo* e il grassetto.

### qusto è un titolo di livello 3
Con il suo testo e:
- elenco puntato
- ...

o anche elenco numerato:
1. primo
2. secondo