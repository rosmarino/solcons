# Solcons

Solutions Consulting corporate website

## release 0.1 - initial commit
- pipeline .gitlab-ci.yml per sito puro HTML 

## release 0.2 - creazione sito Jekyll da local
- elminazione .gitlab-ci.yml
- comandi Ruby:
```
git init (initial pull)
bundle init
bundle add jekyll
bundle exec jekyll new --force --skip-bundle .
bundle install
bundle exec jekyll serve
```

OK:
- aggiungere a .gtignore: `SolutionsConsulting.code-workspace` e `.DS_Store`
- commit + push
- creazione servizio web su render.com/ con `bundle exec jekyll build`
- sito web live all'indirizzo: https://solcons.onrender.com

## release 0.3 - creazione struttura del sito

basato sul sito di sviluppo: https://gitlab.com/rosmarino/coll-test

In particolare:
- collections: "team-members", "services" e "sections"
- assets: CSS, JS, images

26 ottobre 2020: repository aggiunto a MacBookAir